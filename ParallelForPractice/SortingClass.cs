﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace ParallelForPractice
{
    struct Array
    {
        public int[] arrayOfNum;
    }

    class SortingClass
    {
        static int[] SortInsertMethod(object arg)
        {
            Thread.Sleep(2000);

            int[] numArray = ((Array)arg).arrayOfNum;
            int x, j;

            Parallel.For(0, numArray.Length, i =>
            {
                x = numArray[i];

                for (j = i - 1; j >= 0 && numArray[j] > x; j--)
                    numArray[j + 1] = numArray[j];

                numArray[j + 1] = x;
            }
            );

            return numArray;
        }

        static int[] BubbleSort(object arg)
        {
            int[] numArray = ((Array)arg).arrayOfNum;
            int temp;

            Parallel.For(0, numArray.Length, i =>
            {
                for (int j = i + 1; j < numArray.Length; j++)
                {
                    if (numArray[i] > numArray[j])
                    {
                        temp = numArray[i];
                        numArray[i] = numArray[j];
                        numArray[j] = temp;
                    }
                }
            }
            );

            return numArray;
        }

        public async Task<int[]> AsyncOperation(int[] numberArray)
        {
            Task<int[]> task2;
            Array array;

            array.arrayOfNum = numberArray;

            task2 = new Task<int[]>(SortInsertMethod, array);
            task2.Start();
            ShowSortingArray(task2.Result);

            Task<int[]> task1 = new Task<int[]>(BubbleSort, array);
            task1.Start();

            return await task1;
        }

        public static void ShowSortingArray(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }
            Console.WriteLine("Method ended.");
        }
    }
}
