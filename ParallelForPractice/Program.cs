﻿using System;
using System.Threading.Tasks;

namespace ParallelForPractice
{
    class Program
    {
        static void Main(string[] args)
        {
            ParallelOptions options = new ParallelOptions();
            options.MaxDegreeOfParallelism = 2; 

            int[] numbers = new int[50] { 34, -45, 56, 67, 5, 6, -4, 7, 3, 8, 34, -45, 56, 67, 5, 6, -4, 7, 3, 8, 34, -45, 56, 67, 5, 6, -4, 7, 3, 8, 34, -45, 56, 67, 5, 6, -4, 7, 3, 8, 34, -45, 56, 67, 5, 6, -4, 7, 3, 8 };

            SortingClass instance = new SortingClass();

            Console.WriteLine("Task 1: There is a large array of numbers. Sort the array numbers in one Task by the bubble method, in the other - by the inserts method." +
                              "Both Tasks have to perform only two processors. For sorting use the loop Parralel.For. Try to use async await.");

            Task<int[]> task = instance.AsyncOperation(numbers);
            SortingClass.ShowSortingArray(task.Result);

            Console.ReadKey();
        }
    }
}
